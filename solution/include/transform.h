#include "image.h"

#ifndef LAB3_TRANSFORM_H
#define LAB3_TRANSFORM_H

image rotate(image const* img);

#endif //LAB3_TRANSFORM_H

