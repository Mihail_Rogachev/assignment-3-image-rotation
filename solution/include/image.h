#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H

#include <stdint.h>


#pragma pack(push, 1)
typedef struct pixel 
{
    uint8_t b, g, r;
} pixel;
#pragma pack(pop)



typedef struct image{
    uint64_t width, height;
    pixel* data;
} image;


image constructor(uint64_t width, uint64_t height);
void destroy(image* img);


#endif //LAB3_IMAGE_H
