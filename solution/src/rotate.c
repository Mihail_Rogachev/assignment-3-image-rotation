#include "../include/transform.h"
#include <stdio.h>


image rotate(image const* img){
    image new_img = constructor(img->height, img->width);
    for (size_t i = 0; i < img->height; i++){
        for (size_t j = 0; j < img->width; j++){
            new_img.data[img->height * j + img->height - i - 1] = img->data[img->width * i + j];
        }
    }

    return new_img;
}
