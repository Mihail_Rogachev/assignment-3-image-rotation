#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include "../include/bmp.h"
#include "../include/transform.h"


int main(int argc, char* argv[]) {

    if (argc != 3){
        fprintf(stderr, "Incorrect input!\n");
        return EX_DATAERR;
    }

    FILE* input_file = fopen(argv[1], "rb");
    if (!input_file){
        fprintf(stderr, "Can't open input file!");
        return EX_NOINPUT;
    }

    FILE* output_file = fopen(argv[2], "wb");
    if (!output_file){
        fprintf(stderr, "Can't open output file!");
        fclose(input_file);
        return EX_CANTCREAT;
    }
    image img = {0};
    if (from_bmp(input_file, &img) != READ_OK) {
        fprintf(stderr, "Error: can't read image!\n");
        fclose(input_file);
        fclose(output_file);
        destroy(&img);
        return EX_IOERR;
    }


    image rotated_img = rotate(&img);

    if (to_bmp(output_file, &rotated_img)) {
        fprintf(stderr, "Error: can't convert to bmp!\n");
        fclose(input_file);
        fclose(output_file);
        destroy(&img);
        destroy(&rotated_img);
        return EX_IOERR;
    }

    fclose(input_file);
    fclose(output_file);
    destroy(&img);
    destroy(&rotated_img);

    return 0;
}

