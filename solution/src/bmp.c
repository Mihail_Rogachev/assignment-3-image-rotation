#include <stdio.h>

#include "../include/bmp.h"

#define TYPE 0x4D42
#define OFFBITS 54
#define SIZE 40
#define PLANES 1
#define BITCOUNT 24
#define ZERO 0




inline static uint8_t calculate_padding(uint64_t width){
    return (4 - (sizeof(pixel) * width)) % 4;
}

inline static uint32_t calculate_image_size(image const* img){
    return (sizeof(pixel) * img->width + calculate_padding(img->width)) * img->height;
}

inline static uint32_t calculate_file_size(image const* img){
    return 54 + calculate_image_size(img);
}

inline static struct bmp_header generate_BMPheader(image const* img){
    return (struct bmp_header) {
       .bfType = TYPE,
       .bfileSize = calculate_file_size(img),
       .bfReserved = ZERO,
       .bOffBits = OFFBITS,
       .biSize = SIZE,
       .biWidth = img->width,
       .biHeight = img->height,
       .biPlanes = PLANES,
       .biBitCount = BITCOUNT,
       .biCompression = ZERO,
       .biSizeImage = calculate_image_size(img),
       .biXPelsPerMeter = ZERO,
       .biYPelsPerMeter = ZERO,
       .biClrUsed = ZERO,
       .biClrImportant = ZERO
    };
}

enum read_status from_bmp(FILE* in, image* img){
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    *img = constructor(header.biWidth, header.biHeight);
    if (!img->data) return READ_INVALID_SIGNATURE;
    uint8_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; i++){
        if(fread(img->data + i * img->width, sizeof(pixel), img->width, in) != img->width) return READ_INVALID_SIGNATURE;

        if (fseek(in, padding, SEEK_CUR) != 0) return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, image const* img ){
    struct bmp_header header = generate_BMPheader(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;

    for (size_t i = 0; i < img->height; i++ ){
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) return WRITE_ERROR;

        if (fseek(out, calculate_padding(img->width), SEEK_CUR) != 0) return WRITE_ERROR;
    }
    return WRITE_OK;
}




