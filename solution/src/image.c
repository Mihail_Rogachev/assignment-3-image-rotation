#include <malloc.h>

#include "../include/image.h"

image constructor(uint64_t width, uint64_t height){
    pixel* pxls = (pixel*) malloc(sizeof(pixel) * width * height);
    image img = {.height = height,
                 .width = width,
                 .data = pxls
                 };
    return img;
}

void destroy(image* img){
    free(img->data);
}





